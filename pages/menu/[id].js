import {fetchAPI} from "../../utils/api";
import Head from "next/head";

const Menu = ({ menu }) => {
  return (
    <div>
      <Head>
        <title>{ menu.attributes.title }</title>
      </Head>

      <h1>{ menu.attributes.title }</h1>
      <p>
        <strong>Descripció</strong><br />
        { menu.attributes.description }
      </p>
      { menu.attributes.seccions.map(seccio => (
        <div key={seccio.id}>
          <h2>{ seccio.title }</h2>
          { seccio.plats.data.map(plat => (
            <div key={plat.id}>
              <h3>{ plat.attributes.title }</h3>
              <p>
                <strong>Descripció</strong><br />
                { plat.attributes.description }
              </p>
              <p>
                <strong>Preu</strong><br />
                { plat.attributes.preu }
              </p>
            </div>
          )) }
        </div>
      )) }
    </div>
  );
}

export async function getStaticPaths() {
  const menus = await fetchAPI('/menus')

  return {
    paths: menus.data.map((menu) => ({
      params: {
        id: menu.id.toString(),
      }
    })),
    fallback: false
  }
}

export async function getStaticProps({ params }) {
  const menu = await fetchAPI(`/menus/${params.id}?populate=seccions.plats`)
  console.log(menu)

  return {
    props: {
      menu: menu.data
    },
    revalidate: 1
  }
}

export default Menu;
